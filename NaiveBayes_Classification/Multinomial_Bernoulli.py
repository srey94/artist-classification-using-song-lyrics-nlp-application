#Authors : Hari Arunachalam and Sreyas Naaraayanan

#Libraries Import
from nltk import FreqDist
from nltk.classify import accuracy
from nltk.corpus.reader import CategorizedPlaintextCorpusReader
import random
from nltk.classify import SklearnClassifier
from sklearn.naive_bayes import BernoulliNB,MultinomialNB
from sklearn.svm import SVC

#Preparing the Categorized Artist Corpus Reader


reader = CategorizedPlaintextCorpusReader('./Cleaned/set', r'so_.*\.txt',
cat_map={'so_a_drake.txt': ['drake'],'so_a_eminem.txt': ['eminem'],'so_a_dua-lipa.txt': ['dua-lipa'],'so_a_frank-ocean.txt': ['frank-ocean'],'so_a_kendrick-lamar.txt': ['kendrick-lamar'],
         'so_b_drake.txt': ['drake'],'so_b_eminem.txt': ['eminem'],'so_b_dua-lipa.txt': ['dua-lipa'],'so_b_frank-ocean.txt': ['frank-ocean'],'so_b_kendrick-lamar.txt': ['kendrick-lamar'],
         'so_c_drake.txt': ['drake'],'so_c_eminem.txt': ['eminem'],'so_c_dua-lipa.txt': ['dua-lipa'],'so_c_frank-ocean.txt': ['frank-ocean'],'so_c_kendrick-lamar.txt': ['kendrick-lamar'],
         'so_d_drake.txt': ['drake'],'so_d_eminem.txt': ['eminem'],'so_d_dua-lipa.txt': ['dua-lipa'],'so_d_frank-ocean.txt': ['frank-ocean'],'so_d_kendrick-lamar.txt': ['kendrick-lamar'],
         'so_e_drake.txt': ['drake'],'so_e_eminem.txt': ['eminem'],'so_e_dua-lipa.txt': ['dua-lipa'],'so_e_frank-ocean.txt': ['frank-ocean'],'so_e_kendrick-lamar.txt': ['kendrick-lamar'],

         })


documents = [(list(reader.words(fileid)), category)
        for category in reader.categories()

                for fileid in reader.fileids(category)]

#Finding Frequency Distribution
all_words = FreqDist(w.lower() for w in reader.words())

random.shuffle(documents)

#Method to create  Featureset required by Bernoulli Naive Bayes Classifier
word_features = list(all_words)
def document_features(document):
        document_words = set(document)
        features = {}
        for word in word_features:
                features['{}'.format(word)] = 1 if word in document_words else 0
                
        return features

# Method to create  Featureset required by Multinomial Naive Bayes

def document_features2(document):
        document_words = set(document)
        features = {}
        for word in word_features:
                #features['{}'.format(word)] = (word in document_words)
                features['{}'.format(word)] = all_words[word]
        return features


featuresets = [(document_features(d), c) for (d,c) in documents]

#Creating Training and Test Data Set
train_set, test_set = featuresets[6:], featuresets[:4]

# Feeding Data to Classifier
classif = SklearnClassifier(BernoulliNB()).train(train_set)

#Classifier Accuracy Calculation
print("Accuracy of Bernoulli Naive Bayes: " +str(accuracy(classif, test_set)))

featuresets = [(document_features2(d), c) for (d,c) in documents]
train_set, test_set = featuresets[6:], featuresets[:4]

# Feeding Data to Classifier
classif2 = SklearnClassifier(MultinomialNB()).train(train_set)

#Classifier Accuracy Calculation
print("Accuracy of Multinomial Naive Bayes: "+str(accuracy(classif2, test_set)))
