#Authors : Hari Arunachalam and Sreyas Naaraayanan

#Libraries Import
from nltk import FreqDist, NaiveBayesClassifier
from nltk.classify import accuracy
import random

from nltk.corpus.reader import CategorizedPlaintextCorpusReader

#Preparing the Categorized Artist Corpus Reader

reader = CategorizedPlaintextCorpusReader('./Cleaned/set', r'so_.*\.txt',
cat_map={'so_a_drake.txt': ['drake'],'so_a_eminem.txt': ['eminem'],'so_a_dua-lipa.txt': ['dua-lipa.txt'],'so_a_frank-ocean.txt': ['frank-ocean'],'so_a_kendrick-lamar.txt': ['kendrick-lamar'],
        'so_b_drake.txt': ['drake'],'so_b_eminem.txt': ['eminem'],'so_b_dua-lipa.txt': ['dua-lipa.txt'],'so_b_frank-ocean.txt': ['frank-ocean'],'so_b_kendrick-lamar.txt': ['kendrick-lamar'],
         'so_c_drake.txt': ['drake'],'so_c_eminem.txt': ['eminem'],'so_c_dua-lipa.txt': ['dua-lipa.txt'],'so_c_frank-ocean.txt': ['frank-ocean'],'so_c_kendrick-lamar.txt': ['kendrick-lamar'],
         'so_d_drake.txt': ['drake'],'so_d_eminem.txt': ['eminem'],'so_d_dua-lipa.txt': ['dua-lipa.txt'],'so_d_frank-ocean.txt': ['frank-ocean'],'so_d_kendrick-lamar.txt': ['kendrick-lamar'],
         'so_e_drake.txt': ['drake'],'so_e_eminem.txt': ['eminem'],'so_e_dua-lipa.txt': ['dua-lipa.txt'],'so_e_frank-ocean.txt': ['frank-ocean'],'so_e_kendrick-lamar.txt': ['kendrick-lamar'],

         })

documents = [(list(reader.words(fileid)), category)
	for category in reader.categories()

		for fileid in reader.fileids(category)]
random.shuffle(documents)

#Finding Frequency Distribution

all_words = FreqDist(w.lower() for w in reader.words())

word_features = list(all_words)

#Method to create  Featureset required by the classifier
def document_features(document):
	document_words = set(document)
	features = {}
	for word in word_features:
		features['contains({})'.format(word)] = (word in document_words)
	return features
    
featuresets = [(document_features(d), c) for (d,c) in documents]
rtemFree = str(featuresets).encode('utf-8','ignore').decode('utf-8')

#Creating Training and Test Data Set
train_set, test_set = featuresets[4:], featuresets[:4]

# Feeding Data to Classifier
classifier = NaiveBayesClassifier.train(train_set)

#Classifier Accuracy Calculation
print(accuracy(classifier, test_set))
