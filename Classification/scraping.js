var jq = document.createElement('script');
jq.src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js";
document.getElementsByTagName('head')[0].appendChild(jq);
var json = {};
var array = [];

function getLength() {
    console.log(jQuery("artist-songs scrollable-data transclude-injecting-local-scope").length);
}

function scrapeForArtist(artist) {
    if (!artist) {
        console.log("Artist not specified");
        return;
    }
    var length = jQuery("artist-songs scrollable-data transclude-injecting-local-scope").length;
    if (length < 100) {
        console.log("Not enough songs: " + length);
        return;
    }
    var urls = [];
    jQuery("artist-songs scrollable-data transclude-injecting-local-scope").each(function(index, element) {
        var url = jQuery(this).find("a").attr("href");
        urls[urls.length] = url;
    });


    for (var index in urls) {
        var url = urls[index];
        lyric(url, artist, function(json) {
            array[array.length] = json;
        });
    }

    json["output"] = array;

}

function lyric(url, artist, callback) {
    var json = {};
    ajax(url, function(data) {
        var lyrics = jQuery(data).find(".lyrics").text().trim().split(/\s+/).join(" ");
        json["artist"] = artist;
        json["lyrics"] = lyrics;
        json["url"] = url;
        if (callback) {
            callback(json);
        }
    });
}

function ajax(url, callback) {
    jQuery.ajax({
        url: url,
        success: function(result) {
            if (callback) {
                callback(result);
            }
        }
    });
}

var download = function(text, name) {
    name = name.toLowerCase().split(" ").join("-");
    var data = new Blob([text], { type: 'text/plain' });
    var textFile = window.URL.createObjectURL(data);

    var link = document.createElement('a');
    link.setAttribute('download', name + '.json');
    link.href = textFile;
    document.body.appendChild(link);

    var event = new MouseEvent('click');
    link.dispatchEvent(event);
    document.body.removeChild(link);

    window.URL.revokeObjectURL(textFile);
    return textFile;
};