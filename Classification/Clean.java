import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONObject;

public class Clean {
	public static void main(String[] args) {
		System.out.println("Start");
		try {
			start();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("End");
	}

	private static void start() throws IOException {
	File file = new File("D:\\\\jWorkspace\\\\NLP\\\\src\\\\test.txt");
//		if (!file.exists()) {
//			System.out.println("No folder found with name 'Scrape'");
//			return;
//		}
//		if (!file.isDirectory()) {
//			System.out.println("'Scrape' is not a directory");
//			return;
//		}
//		File[] files = file.listFiles();
		Map<String, List<String>> masterMap = new TreeMap<>();
//		for (File eachFile : files) {
			Map<String, List<String>> cleanArtistFile = cleanArtistFile(
					readFile(file));
			JSONObject json = new JSONObject();
//			System.out.println(eachFile.getName());
//			System.out.println(cleanArtistFile.keySet());
			for (String artist : cleanArtistFile.keySet()) {
				JSONArray unigrams = new JSONArray();
				json.put(artist, unigrams);
				for (String unigram : cleanArtistFile.get(artist)) {
					unigrams.put(unigram);
				}
				if (masterMap.containsKey(artist)) {
					masterMap.get(artist).addAll(cleanArtistFile.get(artist));
				} else {
					masterMap.put(artist, cleanArtistFile.get(artist));
				}
			}
			String outputFileName = "test"
					+ "-cleaned.json";
			writeToFile("cleaned/" + outputFileName,
					json.toString().getBytes());
//		}
		JSONObject mjson = new JSONObject();
		for (String artist : masterMap.keySet()) {
			System.out.println(artist + " : " + masterMap.get(artist).size());
			JSONArray unigrams = new JSONArray();
			mjson.put(artist, unigrams);
			for (String unigram : masterMap.get(artist)) {
				unigrams.put(unigram);
			}
		}
		writeToFile("cleaned/master.json", mjson.toString().getBytes());
//		String jsonStr = readFile(new File("/tmp/t.json"));
//		cleanArtistFile(jsonStr);
	}

	private static Map<String, List<String>> cleanArtistFile(
			String fileContent) {
		JSONObject json = new JSONObject(fileContent);
		JSONArray allSongs = json.getJSONArray("output");
		Map<String, List<String>> artistVsUnigrams = new HashMap<>();
		for (int i = 0; i < allSongs.length(); i++) {
			Map<String, List<String>> result = cleanSong(
					allSongs.getJSONObject(i));
			for (String artist : result.keySet()) {
				if (artistVsUnigrams.containsKey(artist)) {
					artistVsUnigrams.get(artist).addAll(result.get(artist));
				} else {
					artistVsUnigrams.put(artist, result.get(artist));
				}
			}
		}
		return artistVsUnigrams;
	}

	private static Map<String, List<String>> cleanSong(JSONObject song) {
		String lyrics = song.getString("lyrics");
		String artist = song.getString("artist");
		Set<String> artists = new HashSet<>();
		artists.add(artist.toLowerCase());
		Map<String, List<String>> artistVsVerses = new HashMap<>();
		Map<String, List<String>> artistVsUnigrams = new HashMap<>();
		char[] charArray = lyrics.toCharArray();
		boolean open = false;
		StringBuilder inside = new StringBuilder();
		StringBuilder verse = new StringBuilder();
		for (char c : charArray) {
			if (open) {
				if (c != ']') {
					inside.append(c);
				} else {
					open = false;
					if (!verse.toString().isEmpty()) {
						for (String eachArtist : artists) {
							eachArtist = eachArtist.trim();
							List<String> versesForArtist = artistVsVerses
									.get(eachArtist);
							if (versesForArtist == null) {
								versesForArtist = new ArrayList<>();
								artistVsVerses.put(eachArtist, versesForArtist);
							}
							versesForArtist.add(verse.toString());
							verse = new StringBuilder();
						}
					}
					String word = inside.toString();
					Set<String> tartists = cleanArtistName(word);
					if (!tartists.isEmpty()) {
						artists = tartists;
					}
//					System.out.println("----------" +inside);
//					System.out.println(artists);
					inside = new StringBuilder();
				}
			} else {
				if (c == '[') {
					open = true;
				} else {
					verse.append(c);
				}
			}
		}
		if (!verse.toString().isEmpty()) {
			List<String> versesForArtist = artistVsVerses.get(artist);
			if (versesForArtist == null) {
				versesForArtist = new ArrayList<>();
				artistVsVerses.put(artist, versesForArtist);
			}
			versesForArtist.add(verse.toString());
			verse = new StringBuilder();
		}
		for (String eachArtist : artistVsVerses.keySet()) {
			List<String> verses = artistVsVerses.get(eachArtist);
			List<String> unigrams = new ArrayList<>();
			for (String eachVerse : verses) {
				for (String word : eachVerse.split("[^\\w']+")) {
					if (!word.trim().isEmpty()) {
						unigrams.add(word.trim().toLowerCase());
					}
				}
			}
			if (!unigrams.isEmpty()) {
				artistVsUnigrams.put(eachArtist, unigrams);
			}
		}
		return artistVsUnigrams;
	}

	private static String readFile(File file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		try {
			String line = null;
			StringBuilder output = new StringBuilder();
			while ((line = reader.readLine()) != null) {
				output.append(line).append(System.lineSeparator());
			}
			return output.toString().trim();
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}

	public static void writeToFile(String filePath, byte[] content)
			throws IOException {
		File file = new File(filePath);
		FileOutputStream fos = null;
		try {
			if (file.exists()) {
				file.delete();
			}
			file.getParentFile().mkdirs();
			file.createNewFile();
			fos = new FileOutputStream(file);
			fos.write(content);
		} finally {
			fos.close();
		}
	}

	private static Set<String> cleanArtistName(String word) {
		Set<String> artists = new HashSet<>();
		if (word.contains(":")) {
			if (word.split(":")[0] != "Verse") {
				String tArtist = word.split(":")[1].replaceAll("\"", "").trim();
				if (tArtist.startsWith("\"")
						&& !word.split(":")[0].trim().startsWith("Verse")) {
					artists.add(word.split(":")[0].trim().toLowerCase());
				} else if (!tArtist.trim().isEmpty()) {
					String allArtists = tArtist.trim();
					for (String eachArtist : allArtists
							.split("&|with|\\+|,|/|\\(|\\)")) {
						eachArtist = eachArtist.replaceAll("Both", "").trim();
						if (!eachArtist.isEmpty()) {
							artists.add(eachArtist.toLowerCase());
						}
					}
				}
			}
		}
		return artists;
	}
}
