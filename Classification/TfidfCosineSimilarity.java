import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TfidfCosineSimilarity {
	private static final String filePath = "D:\\jWorkspace\\NLP\\src\\master.json";
	private static final String testFilePath = "D:\\jWorkspace\\NLP\\src\\cleaned-test.txt";
	public static void main(String[] args) {
		long stime = System.currentTimeMillis();
		System.out.println("Start");
		try {
			int n = 1;
			if (args != null && args.length == 1) {
				try {
					n = Integer.parseInt(args[0]);
				} catch (Exception e) {
					System.out.println("Invalid value of n");
				}
			}
			ngramClassify(n, 10);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("End");
		System.out.println("Running time : " + (System.currentTimeMillis() - stime) + " ms");
	}

	private static Map<String, Double> idf() throws JSONException, IOException {
		JSONObject json = new JSONObject(readFile(filePath));
		@SuppressWarnings("unchecked")
		Set<String> artists = json.keySet();
		Map<String, Set<String>> unigramVsArtists = new HashMap<>();
		for (String artist : artists) {
			JSONArray unigrams = json.getJSONArray(artist);
			for (int i = 0; i < unigrams.length(); i++) {
				String unigram = unigrams.getString(i);
				Set<String> artistsForUnigram = unigramVsArtists.get(unigram);
				if (artistsForUnigram == null) {
					artistsForUnigram = new HashSet<>();
					unigramVsArtists.put(unigram, artistsForUnigram);
				}
				artistsForUnigram.add(artist);
			}
		}
		Map<String, Double> unigramVsIdf = new HashMap<>();
		final Integer totalArtists = artists.size();
		for (String unigram : unigramVsArtists.keySet()) {
			final int artistsCount = unigramVsArtists.get(unigram).size();
			unigramVsIdf.put(unigram,
					Math.log(totalArtists / (double) artistsCount));
		}
		return unigramVsIdf;
	}

	private static void ngramClassify(int n, int roof)
			throws JSONException, IOException {
		JSONObject t_json = new JSONObject(readFile(testFilePath));
		Map<String,Double> testMap = new TreeMap<>();
		int testSize =0 ;
		for (Object artist : t_json.keySet()) {
		JSONArray unigrams = t_json.getJSONArray(artist.toString());
		List<String> nGrams = getNGrams(n, unigrams);
		testSize = nGrams.size();
		testMap = printTfIdfTable(nGrams, testSize);
		System.out.println("Test Artist: " + artist);
		System.out.println("-------------------------");
//		for(String ng:testMap.keySet()) {
//			System.out.println(print(ng, 3) + testMap.get(ng));
//		}
		System.out.println("-------------------------\n\n");
		}
		
		String artistName ="";
		Double euclideanDistance =999999.9999;
		
		JSONObject json = new JSONObject(readFile(filePath));
		Map<String,Double> finalMap = new TreeMap<>();
		for (Object artist : json.keySet()) {
			JSONArray unigrams = json.getJSONArray(artist.toString());
			if (unigrams.length() < 30000) {
				continue;
			}
			List<String> nGrams = getNGrams(n, unigrams);
			System.out.println("Artist: " + artist);
			System.out.println("---------------------------------------------------------------------------------------------");
//			printFreqTable(nGrams, roof);
			finalMap= printTfIdfTable(nGrams, testSize);
			System.out.println("---------------------------------------------------------------------------------------------\n\n");
			
			Double euDist = cosine(finalMap, testMap);
			if(euDist<euclideanDistance) {
				euclideanDistance= euDist;
				artistName = artist.toString();
			}
		}
		
		System.out.println("The most probable artist is "+ artistName+ " with a distance of "+ euclideanDistance);

	}

	private static Map<String,Double> printTfIdfTable(List<String> ngrams, int roof)
			throws JSONException, IOException {
		long stime = System.currentTimeMillis();
		Map<String, Double> idf = idf();
		Map<Double, Set<String>> rmap = new TreeMap<>(
				Collections.reverseOrder());
		Map<String, Integer> ngramCount = new HashMap<>();
		Map<String,Double> finalMap = new TreeMap<>();
		for (String ngram : ngrams) {
			Integer count = ngramCount.get(ngram);
			if (count == null) {
				count = 0;
			}
			ngramCount.put(ngram, count + 1);
		}
		System.out.println("NGram Frequency running time: " + (System.currentTimeMillis() - stime) + " ms");
		stime = System.currentTimeMillis();
		for (String unigram : idf.keySet()) {
			if (ngrams.contains(unigram)) {
				Set<String> allUnigrams = rmap.get(idf.get(unigram));
				if (allUnigrams == null) {
					allUnigrams = new TreeSet<>();
					rmap.put(ngramCount.get(unigram) * idf.get(unigram), allUnigrams);
				}
				allUnigrams.add(unigram);
			}
		}
		System.out.println("Sort running time: " + (System.currentTimeMillis() - stime) + " ms");
		System.out.println("Total ngrams: " + ngrams.size());
		System.out.println(
				"NGRAM\t\t\tFREQUENCY\t\tIDF\t\t\t\tTFIDF");
		System.out.println(
				"---------------------------------------------------------------------------------------------");
		boolean exit = false;
		int i = 0;
		for (Double tfidf : rmap.keySet()) {
			for (String unigram : rmap.get(tfidf)) {
				finalMap.put(unigram,tfidf);
				System.out.println(print(unigram, 3) + ngramCount.get(unigram) + "\t\t" + idf.get(unigram) + "\t\t" + tfidf);
				i++;
				if (i>= roof) {
					exit = true;
					break;
				}
			}
			if (exit) {
				break;
			}
		}
		return finalMap;
	}

	private static void printFreqTable(List<String> ngrams, int roof)
			throws JSONException, IOException {
		int total = ngrams.size();
		Map<String, Double> idf = idf();
		Map<String, Integer> ngramCount = new HashMap<>();
		for (String ngram : ngrams) {
			Integer count = ngramCount.get(ngram);
			if (count == null) {
				count = 0;
			}
			ngramCount.put(ngram, count + 1);
		}
		Map<Integer, Set<String>> sortedMap = new TreeMap<>(
				Collections.reverseOrder());
		for (String ngram : ngramCount.keySet()) {
			Integer count = ngramCount.get(ngram);
			if (sortedMap.containsKey(count)) {
				sortedMap.get(count).add(ngram);
			} else {
				sortedMap.put(count, new HashSet<>());
				sortedMap.get(count).add(ngram);
			}
		}
		int i = 0;
		System.out.println("Total ngrams: " + total);
		System.out.println(
				"NGRAM\t\t\tFREQUENCY\t\tFREQ_DIST\t\tIDF\t\t\t\tTFIDF");
		boolean exit = false;
		for (Integer count : sortedMap.keySet()) {
			Set<String> set = sortedMap.get(count);
			for (String string : set) {
				Double idfValue = idf.get(string);
				System.out.println(print(string, 3) + count + "\t\t\t"
						+ (count / (float) total) + "\t\t" + idfValue + "\t\t"
						+ (idfValue * count));
				i++;
				if (i >= roof) {
					exit = true;
					break;
				}
			}
			if (exit) {
				break;
			}
		}
	}

	private static String print(String string, int tabs) {
		StringBuilder output = new StringBuilder(string);
		int n = tabs * 8 - string.length();
		for (int i = 0; i < n; i++) {
			output.append(" ");
		}
		return output.toString();
	}

	private static List<String> getNGrams(int n, JSONArray unigrams) {
		List<String> ngrams = new ArrayList<>();
		for (int i = 0, j = n; j < unigrams.length(); i++, j++) {
			StringBuilder ngram = new StringBuilder();
			for (int x = i; x < j; x++) {
				if(unigrams.get(x).toString().length()>3)
				ngram.append(unigrams.get(x)).append(" ");
			}
			ngrams.add(ngram.toString().trim());
		}
		return ngrams;
	}

	private static List<String> getNGrams(int n, String text) {
		String[] words = text.split("\\s+");
		List<String> ngrams = new ArrayList<>();
		for (int i = 0, j = n; j < words.length; i++, j++) {
			StringBuilder ngram = new StringBuilder();
			for (int x = i; x < j; x++) {
			
				ngram.append(words[x]).append(" ");
			}
			ngrams.add(ngram.toString().trim());
		}
		return ngrams;
	}

	static String readFile(String filePath) throws IOException {
		File file = new File(filePath);
		if (!file.exists()) {
			System.out.println(
					"File master.json is not present in folder 'cleaned'");
		}
		BufferedReader reader = new BufferedReader(
				new FileReader(new File(filePath)));
		try {
			String line = null;
			StringBuilder output = new StringBuilder();
			while ((line = reader.readLine()) != null) {
				output.append(line).append(System.lineSeparator());
			}
			return output.toString().trim();
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}
	
	public static Double cosine(Map<String,Double> train,Map<String,Double> test) {
		Double cosineSimilarity =0.0;
		Double numerator = 0.0;
		Double denominatorA = 0.0;
		Double denominatorB = 0.0;
		for(String wg: test.keySet()) {
			Double freq1 = test.get(wg);
			Double freq2 = train.get(wg);
			if(freq2 == null) {
				freq2 =0.0;
			}			
			numerator += freq1 * freq2;
			denominatorA += freq1* freq1;
			denominatorB += freq2* freq2;
			
		}
		
		cosineSimilarity = numerator/ Math.sqrt(denominatorA+denominatorB);
		System.out.println("Euclidean distance"+ cosineSimilarity);
		return cosineSimilarity;
	}
}
