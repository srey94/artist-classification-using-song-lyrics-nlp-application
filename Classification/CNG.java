import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CNG {
	private static final String filePath = "D:\\jWorkspace\\NLP\\src\\master.json";
	private static final String testFilePath = "D:\\jWorkspace\\NLP\\src\\cleaned-test.txt";
	//private static final String resultFilePath = "\"D:\\jWorkspace\\NLP\\src\\result.txt";
	public static void main(String[] args) {
		System.out.println("Start");
		try {
			int n = 1;
			if (args != null && args.length == 1) {
				try {
					n = Integer.parseInt(args[0]);
				} catch(Exception e) {
					System.out.println("Invalid value of n");
				}
			}
		ngramClassify(n, 10);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("End");
	}

	private static void ngramClassify(int n, int roof) throws JSONException, IOException {
		JSONObject t_json = new JSONObject(readFile(testFilePath));
		Map<String,Double> testMap = new TreeMap<>();
		int testSize =0 ;
		for (Object artist : t_json.keySet()) {
		JSONArray unigrams = t_json.getJSONArray(artist.toString());
		List<String> nGrams = getNGrams(n, unigrams);
		testSize = nGrams.size();
		testMap = printFreqTable(nGrams, testSize);
		System.out.println("Test Artist: " + artist);
		System.out.println("-------------------------");
		for(String ng:testMap.keySet()) {
			System.out.println(print(ng, 3) + testMap.get(ng));
		}
		System.out.println("-------------------------\n\n");
		}
		
		//writeToFile(resultFilePath, testMap.toString().getBytes());
		JSONObject json = new JSONObject(readFile(filePath));
		Map<String,Double> finalMap = new TreeMap<>();
		
		
		String artistName ="";
		Double euclideanDistance =999999.9999;
		for (Object artist : json.keySet()) {
			JSONArray unigrams = json.getJSONArray(artist.toString());
			List<String> nGrams = getNGrams(n, unigrams);
			if (unigrams.length() < 30000) {
				continue;
			}
			//if(nGrams.size()>(testSize*5)) {
			finalMap = printFreqTable(nGrams, testSize);
			Double euDist = cng(finalMap, testMap);
			if(euDist<euclideanDistance) {
				euclideanDistance= euDist;
				artistName = artist.toString();
			}
			
			//}
		}
		
		System.out.println("The most probable artist is "+ artistName+ " with a distance of "+ euclideanDistance);
		
	}

	private static Map<String,Double> printFreqTable(List<String> ngrams, int roof) {
		int total = ngrams.size();
		Map<String, Integer> ngramCount = new HashMap<>();
		Map<String,Double> finalMap = new TreeMap<>();
		for (String ngram : ngrams) {
			Integer count = ngramCount.get(ngram);
			if (count == null) {
				count = 0;
			}
			ngramCount.put(ngram, count + 1);
		}
		Map<Integer, Set<String>> sortedMap = new TreeMap<>(
				Collections.reverseOrder());
		for (String ngram : ngramCount.keySet()) {
			Integer count = ngramCount.get(ngram);
			if (sortedMap.containsKey(count)) {
				sortedMap.get(count).add(ngram);
			} else {
				sortedMap.put(count, new HashSet<>());
				sortedMap.get(count).add(ngram);
			}
		}
		int i = 0;
		boolean exit = false;
		
		for (Integer count : sortedMap.keySet()) {
			Set<String> set = sortedMap.get(count);
			for (String string : set) {
				finalMap.put(string,(count / (double)total));
				i++;
				if (i >= roof) {
					exit = true;
					break;
				}
			}
			if (exit) {
				break;
			}
		}
		
		
		return finalMap;
	}

	private static String print(String string, int tabs) {
		StringBuilder output = new StringBuilder(string);
		int n = tabs * 8 - string.length();
		for (int i = 0; i < n; i++) {
			output.append(" ");
		}
		return output.toString();
	}

	private static List<String> getNGrams(int n, JSONArray unigrams) {
		List<String> ngrams = new ArrayList<>();
		for (int i = 0, j = n; j < unigrams.length(); i++, j++) {
			StringBuilder ngram = new StringBuilder();
			for (int x = i; x < j; x++) {
				if(unigrams.get(x).toString().length()>3)
				ngram.append(unigrams.get(x)).append(" ");
			}
			ngrams.add(ngram.toString().trim());
		}
		return ngrams;
	}
	

	static String readFile(String filePath) throws IOException {
		File file = new File(filePath);
		if (!file.exists()) {
			System.out.println("File master.json is not present in folder 'cleaned'");
		}
		BufferedReader reader = new BufferedReader(
				new FileReader(new File(filePath)));
		try {
			String line = null;
			StringBuilder output = new StringBuilder();
			while ((line = reader.readLine()) != null) {
				output.append(line).append(System.lineSeparator());
			}
			return output.toString().trim();
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}
	
	public static void writeToFile(String filePath, byte[] content)
			throws IOException {
		File file = new File(filePath);
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file, true);
			fos.write(content);
		} finally {
			fos.close();
		}
	}
	
	
	public static Double cng(Map<String,Double> train,Map<String,Double> test) {
		Double euDist =0.0;
		for(String wg: test.keySet()) {
			Double freq1 = test.get(wg);
			Double freq2 = train.get(wg);
			if(freq2 == null) {
				freq2 =0.0;
			}else {
				train.remove(wg);
			}
			
			Double numerator = 2 * freq1 - freq2;
			Double denominator = freq1+freq2;
			euDist += (numerator/denominator)*(numerator/denominator);
		}
		for(String wg: train.keySet()) {
			Double freq1 = 0.0;
			Double freq2 = train.get(wg);
						
			Double numerator = 2 * freq1 - freq2;
			Double denominator = freq1+freq2;
			euDist += (numerator/denominator)*(numerator/denominator);
		}
		
		System.out.println("Euclidean distance"+ euDist);
		return euDist;
	}

}
