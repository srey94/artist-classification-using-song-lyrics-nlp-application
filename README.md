## CSCI 4152/6509 Natural Language Processing (Winter 2020)
# P-26: Artist and Genre Classification using Song Lyrics

### Project members

| CSID:          | Name:                 | Course:   | Email:          |
|----------------|-----------------------|-----------|-----------------|
| haria          | Hari Arunachalam           | CSCI 6509 | hr695936@dal.ca |
| sreyas         | Sreyas Naaraayanan Ramanathan | CSCI 6509 | sr744995@dal.ca |
| muthukumar     | Sanjay Muthukumar          | CSCI 6509 | sn752089@dal.ca |
